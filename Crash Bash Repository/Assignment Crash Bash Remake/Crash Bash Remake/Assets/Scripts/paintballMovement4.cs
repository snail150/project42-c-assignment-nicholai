﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paintballMovement4 : MonoBehaviour {
	GameObject bluePlayer;
	float defaultMove = 0f;
	float defaultNegMove = -0f;
	float defaultMove2 = 0f;
	float defaultNegMove2 = 0f;
	public float yellowXPos;

	public AudioClip hitSound;
	public GameObject musicSound;
	public GameObject musicSound2;
	public AudioSource MusicSource;
	public AudioSource MusicSource2;

	GameObject paintballMovement2;
	GameObject paintballMovement3;
	GameObject paintballMovement;
	// Use this for initialization
	void Start () {
		musicSound = GameObject.FindGameObjectWithTag ("BoxSound2");
		musicSound2 = GameObject.FindGameObjectWithTag ("BoxSound3");
		MusicSource = musicSound.GetComponent<AudioSource>();
		MusicSource2 = musicSound2.GetComponent<AudioSource>();
		bluePlayer = GameObject.Find ("BluePlayer");
		paintballMovement2 = GameObject.FindGameObjectWithTag ("GreenPlayer");
		paintballMovement = GameObject.FindGameObjectWithTag ("YellowPlayer");
		paintballMovement3 = GameObject.FindGameObjectWithTag ("RedPlayer");
		StartCoroutine (stages ());
	}

	IEnumerator stages(){
		yield return new WaitForSeconds (4f);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
		defaultMove2 = 2.5f;
		defaultNegMove2 = -2.5f;


	}

	// Update is called once per frame
	void Update ()
	{
		yellowXPos = this.transform.position.x;
		if (Input.GetKey (KeyCode.PageDown)) {
			if (bluePlayer.transform.position.x <= 1400) {
				bluePlayer.transform.Translate (defaultMove, 0f, 0f);
			} else {
				float posy = bluePlayer.transform.position.y;
				bluePlayer.transform.position = new Vector3 (1400f, posy, -1f);
			}		
		} else if (Input.GetKey (KeyCode.Delete)) {
			if (bluePlayer.transform.position.x >= 560) {
				bluePlayer.transform.Translate (defaultNegMove, 0f, 0f);
			} else {
				float posy = bluePlayer.transform.position.y;
				bluePlayer.transform.position = new Vector3 (560f, posy, -1f);
			}
		} else if (Input.GetKey (KeyCode.Home)) {
			if (bluePlayer.transform.position.y <= 948) {
				bluePlayer.transform.Translate (0f, defaultMove, 0f);
			} else {
				float posx = bluePlayer.transform.position.x;
				bluePlayer.transform.position = new Vector3 (posx, 948f, -1f);
			}
		} else if (Input.GetKey (KeyCode.End)) {
			if (bluePlayer.transform.position.y >= 108) {
				bluePlayer.transform.Translate (0f, defaultNegMove, 0f);
			} else {
				float posx = bluePlayer.transform.position.x;
				bluePlayer.transform.position = new Vector3 (posx, 108f, -1f);
			}

		}
	}

	public void blueStunner(){
		StartCoroutine (stunnedSpeed ());
	}

	IEnumerator timedSpeed(){
		defaultMove = 3.5f;
		defaultNegMove = -3.5f;
		yield return new WaitForSeconds (6);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	IEnumerator stunnedSpeed(){
		defaultMove = 0f;
		defaultNegMove = -0f;
		yield return new WaitForSeconds (3);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "SpeedBoost") {
			MusicSource2.Play ();
			Destroy (collision.gameObject);
			StartCoroutine (timedSpeed ());
		}
		if (collision.tag == "StunMine") {
			MusicSource.Play ();
			Destroy (collision.gameObject);
			paintballMovement2.GetComponent<paintballMovement2>().yellowStunner();
			paintballMovement.GetComponent<paintballMovement>().greenStunner();
			paintballMovement3.GetComponent<paintballMovement3>().redStunner();
		}
	}
}
