﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour {
	public GameObject myBallPrefab;
	public GameObject myWallPrefab;
	public GameObject myWall2Prefab;
	GameObject myBallClone;
	GameObject myWallClone;
	GameObject myWallClone2;
	private Vector2 vel;
	private float speed =100;
	int randomSpawn;
	private Rigidbody2D rb2d;
	int count = 0;
	float stage = 5.0f;

	public GUISkin layout;

	public int redScore = 15;
	public int blueScore = 15;
	public int greenScore = 15;
	public int yellowScore = 15;

	GameObject redShip;
	GameObject blueShip;
	GameObject greenShip;
	GameObject yellowShip;

	GameObject bottomWall;
	GameObject topWall;
	GameObject leftWall;
	GameObject rightWall;



	IEnumerator stages(){
		for (int i = 0; i <= 70; i++) {
			Invoke("ballSpawner", 4);
			yield return new WaitForSeconds (stage);
		}
	}


	// Use this for initialization
	void Start () {
		GameObject.FindGameObjectWithTag ("Music").GetComponent<InstructionsManager>().PlayMusic();
		StartCoroutine (stages ());

		redShip = GameObject.Find ("RedShip");
		greenShip = GameObject.Find ("GreenShip");
		blueShip = GameObject.Find ("BlueShip");
		yellowShip = GameObject.Find ("YellowShip");

		bottomWall = GameObject.Find ("BottomGoal");
		topWall = GameObject.Find ("TopGoal");
		leftWall = GameObject.Find ("LeftGoal");
		rightWall = GameObject.Find ("RightGoal");

	}

	public void redPlayerLoseLife(){
		redScore--;
		if (redScore == 0) {
			Destroy (redShip);
			Destroy (rightWall);
			myWallClone2 = Instantiate (myWall2Prefab, transform.position = new Vector3 (1360, 520, 1), Quaternion.Euler (0,0, -89)) as GameObject;
			if (yellowScore <= 0 && blueScore <= 0  && redScore <= 0) {
				SceneManager.LoadScene ("GreenWins");
			} else if (yellowScore <= 0 && greenScore <= 0 && redScore <= 0) {
				SceneManager.LoadScene ("BlueWins");
			} else if (blueScore <= 0 && greenScore <= 0 && redScore <= 0) {
				SceneManager.LoadScene ("YellowWins");
			}
				
		}
	}

	public void greenPlayerLoseLife(){
		greenScore--;
		if (greenScore == 0) {
			Destroy (greenShip);
			Destroy (leftWall);
			myWallClone2 = Instantiate (myWall2Prefab, transform.position = new Vector3 (480, 520,1 ),Quaternion.Euler (0,0, -89)) as GameObject;
			if (yellowScore <= 0 && blueScore <= 0 && greenScore <= 0) {
				SceneManager.LoadScene ("RedWins");
			} else if (yellowScore <= 0 && redScore <= 0 && greenScore <= 0) {
				SceneManager.LoadScene ("BlueWins");
			} else if (blueScore <= 0 && redScore <= 0 && greenScore <= 0) {
				SceneManager.LoadScene ("YellowWins");
			}
		}
	}

	public void bluePlayerLoseLife(){
		blueScore--;
		if (blueScore == 0) {
			Destroy (blueShip);
			Destroy (topWall);
			myWallClone = Instantiate (myWallPrefab, transform.position = new Vector3 (920, 980,1 ), Quaternion.identity) as GameObject;
			if (yellowScore <= 0 && redScore <= 0 && blueScore <= 0) {
				SceneManager.LoadScene ("GreenWins");
			} else if (yellowScore <= 0 && greenScore <= 0 && blueScore <= 0) {
				SceneManager.LoadScene ("RedWins");
			} else if (redScore <= 0 && greenScore <= 0 && blueScore <= 0) {
				SceneManager.LoadScene ("YellowWins");
			}
		}
	}

	public void yellowPlayerLoseLife(){
		yellowScore--;
		if (yellowScore == 0) {
			Destroy (yellowShip);
			Destroy (bottomWall);
			myWallClone = Instantiate (myWallPrefab, transform.position = new Vector3 (920, 100, 1), Quaternion.identity) as GameObject;
			if (redScore <= 0 && blueScore <= 0 && yellowScore <= 0) {
				SceneManager.LoadScene ("GreenWins");
			} else if (redScore <= 0 && greenScore <= 0 && yellowScore <= 0) {
				SceneManager.LoadScene ("BlueWins");
			} else if (blueScore <= 0 && greenScore <= 0 && yellowScore <= 0) {
				SceneManager.LoadScene ("RedWins");
			}
		}
	}






	void ballSpawner(){
		randomSpawn = Random.Range (1, 5);
		if (randomSpawn == 1) {
			myBallClone = Instantiate (myBallPrefab, transform.position = new Vector2 (1270, 200), Quaternion.identity) as GameObject;
			rb2d = myBallClone.GetComponent<Rigidbody2D> ();
			if (count >= 0 && count <= 3) {
				rb2d.AddForce (new Vector2 (-20, 20));
			} else if (count >= 4 && count <= 6) {
				rb2d.AddForce (new Vector2 (-20, 20));
			} else if (count >= 7 && count <= 10) {
				rb2d.AddForce (new Vector2 (-20, 20));
			} else if (count >= 11) {
				rb2d.AddForce (new Vector2 (-20, 20));
			}
			count++;
		} else if (randomSpawn == 2) {
			myBallClone = Instantiate (myBallPrefab, transform.position = new Vector2 (560, 200), Quaternion.identity) as GameObject;
			rb2d = myBallClone.GetComponent<Rigidbody2D> ();
			if (count >= 0 && count <= 3) {
				rb2d.AddForce (new Vector2 (20, 20));
			} else if (count >= 4 && count <= 6) {
				rb2d.AddForce (new Vector2 (20, 20));
			} else if (count >= 7 && count <= 10) {
				rb2d.AddForce (new Vector2 (20, 20));
			} else if (count >= 11) {
				rb2d.AddForce (new Vector2 (20, 20));
			}
			count++;
		} else if (randomSpawn == 3) {
			myBallClone = Instantiate (myBallPrefab, transform.position = new Vector2 (560, 880), Quaternion.identity) as GameObject;
			rb2d = myBallClone.GetComponent<Rigidbody2D> ();
			if (count >= 0 && count <= 3) {
				rb2d.AddForce (new Vector2 (20, -20));
			} else if (count >= 4 && count <= 6) {
				rb2d.AddForce (new Vector2 (20, -20));
			} else if (count >= 7 && count <= 10) {
				rb2d.AddForce (new Vector2 (20, -20));
			} else if (count >= 11) {
				rb2d.AddForce (new Vector2 (20, -20));
			}
			count++;
		} else {
			myBallClone = Instantiate (myBallPrefab, transform.position = new Vector2 (1270, 880), Quaternion.identity) as GameObject;
			rb2d = myBallClone.GetComponent<Rigidbody2D> ();
			if (count >= 0 && count <= 3) {
				rb2d.AddForce (new Vector2 (-20, -20));
			} else if (count >= 4 && count <= 6) {
				rb2d.AddForce (new Vector2 (-20, -20));
			} else if (count >= 7 && count <= 10) {
				rb2d.AddForce (new Vector2 (-20, -20));
			} else if (count >= 11) {
				rb2d.AddForce (new Vector2 (-20, -20));
			}
			count++;
		}
	}


	void stageRun () {
		for (int i = 0; i <= 70; i++) {
			if (count >= 0 && count <= 3) {
				stage = 4.0f;
			} else if (count >= 4 && count <= 6) {
				stage = 3.0f;
			} else if (count >= 7 && count <= 10) {
				stage = 2.0f;
			} else if (count >= 11 && count <= 19) {
				stage = 1.0f;
			} else if(count >= 20){
				stage = 0.5f;
			}

		}
	}

	void OnGUI (){
		GUI.skin = layout;
		GUI.skin.label.fontSize = 40;
		GUI.contentColor = Color.black;
		GUI.Label (new Rect (Screen.width / 7 - 50 - 48, 115, 100, 100), "" + blueScore);
		GUI.Label (new Rect (Screen.width / 2 + 985 - 248, 115, 100, 100), "" + greenScore);
		GUI.Label (new Rect (Screen.width / 100 + 167 - 12, 855, 100, 100), "" + yellowScore);
		GUI.Label (new Rect (Screen.width / 22 + 1620 - 12, 855, 100, 100), "" + redScore);

	}

	void Update(){
		stageRun ();
	}
}
