﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paintballMovement2 : MonoBehaviour {
	GameObject greenPlayer;
	float defaultMove = 0f;
	float defaultNegMove = -0f;
	float defaultMove2 = 0f;
	float defaultNegMove2 = -0f;

	public AudioClip hitSound;
	public GameObject musicSound;
	public GameObject musicSound2;
	public AudioSource MusicSource;
	public AudioSource MusicSource2;

	GameObject paintballMovement;
	GameObject paintballMovement3;
	GameObject paintballMovement4;
	// Use this for initialization
	void Start () {
		musicSound = GameObject.FindGameObjectWithTag ("BoxSound2");
		musicSound2 = GameObject.FindGameObjectWithTag ("BoxSound3");
		MusicSource = musicSound.GetComponent<AudioSource>();
		MusicSource2 = musicSound2.GetComponent<AudioSource>();
		greenPlayer = GameObject.Find ("GreenPlayer");

		paintballMovement = GameObject.FindGameObjectWithTag ("YellowPlayer");
		paintballMovement3 = GameObject.FindGameObjectWithTag ("RedPlayer");
		paintballMovement4 = GameObject.FindGameObjectWithTag ("BluePlayer");
		StartCoroutine (stages ());


	}

	IEnumerator stages(){
		yield return new WaitForSeconds (4f);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
		defaultMove2 = 2.5f;
		defaultNegMove2 = -2.5f;
	}

	// Update is called once per frame
	void Update ()
	{
		
		if (Input.GetKey (KeyCode.RightArrow)) {
			if (greenPlayer.transform.position.x <= 1400) {
				greenPlayer.transform.Translate (defaultMove, 0f, 0f); 	 
			} else {
				Debug.Log ("STOPPPP");
				float posy = greenPlayer.transform.position.y;
				float posxx = greenPlayer.transform.position.x;
				float posXMin = posxx - 8f;
				greenPlayer.transform.position = new Vector3 (posXMin, posy, -1f);
			}		
		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			if (greenPlayer.transform.position.x >= 560) {
				greenPlayer.transform.Translate (defaultNegMove, 0f, 0f);
			} else {
				Debug.Log ("STAPPPPP");
				float posy = greenPlayer.transform.position.y;
				float posxx = greenPlayer.transform.position.x;
				float posXMin = posxx + 8f;
				greenPlayer.transform.position = new Vector3 (posXMin, posy, -1f);
			}
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			if (greenPlayer.transform.position.y <= 948) {
				greenPlayer.transform.Translate (0f, defaultMove, 0f);
			} else {
				float posx = greenPlayer.transform.position.x;
				greenPlayer.transform.position = new Vector3 (posx, 948f, -1f);
			}
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			if (greenPlayer.transform.position.y >= 108) {
				greenPlayer.transform.Translate (0f, defaultNegMove, 0f);
			} else {
				float posx = greenPlayer.transform.position.x;
				greenPlayer.transform.position = new Vector3 (posx, 108f, -1f);
			}

		}
	}

	IEnumerator timedSpeed(){
		defaultMove = 3.5f;
		defaultNegMove = -3.5f;
		yield return new WaitForSeconds (6);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	IEnumerator stunnedSpeed(){
		
		defaultMove = 0f;
		defaultNegMove = -0f;
		yield return new WaitForSeconds (3);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	public void yellowStunner(){
		StartCoroutine (stunnedSpeed ());
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "SpeedBoost") {
			MusicSource2.Play ();
			Destroy (collision.gameObject);
			StartCoroutine (timedSpeed ());
		}
		if (collision.tag == "StunMine") {
			MusicSource.Play ();
			Destroy (collision.gameObject);
			paintballMovement.GetComponent<paintballMovement> ().greenStunner ();
			paintballMovement3.GetComponent<paintballMovement3> ().redStunner ();
			paintballMovement4.GetComponent<paintballMovement4> ().blueStunner ();
		}
	}
}