﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileColourTrigger : MonoBehaviour {
	public GameObject myYellowTilePrefab;
	GameObject myYellowTileClone;
	public GameObject myRedTilePrefab;
	GameObject myRedTileClone;
	public GameObject myBlueTilePrefab;
	GameObject myBlueTileClone;
	GameObject gameControllerr;
	GameObject neutralTile;
	GameObject yellowTile;
	GameObject greenTile;
	GameObject redTile;
	GameObject blueTile;
	// Use this for initialization
	void Start () {
		gameControllerr = GameObject.FindGameObjectWithTag ("GameController");
		neutralTile = GameObject.FindGameObjectWithTag ("Neutral");
		yellowTile = GameObject.FindGameObjectWithTag ("YellowTile");
		greenTile = GameObject.FindGameObjectWithTag ("GreenTile");
		redTile = GameObject.FindGameObjectWithTag ("RedTile");
		blueTile = GameObject.FindGameObjectWithTag ("BlueTile");

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "YellowPlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myYellowTileClone= Instantiate (myYellowTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

		if (collision.tag == "RedPlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myRedTileClone= Instantiate (myRedTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

		if (collision.tag == "BluePlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myBlueTileClone= Instantiate (myBlueTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}
	}
}
