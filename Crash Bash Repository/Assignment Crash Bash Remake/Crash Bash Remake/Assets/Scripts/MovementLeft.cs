﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementLeft : MonoBehaviour {
	public KeyCode moveUp = KeyCode.W;
	public KeyCode moveDown = KeyCode.S;
	public float speed = 10.0f;
	private Rigidbody2D rb2d;

	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update () {
		var vel = rb2d.velocity;
		if (Input.GetKey(moveUp)) {
			vel.y = speed;
		}
		else if (Input.GetKey(moveDown)) {
			vel.y = -speed;
		}
		else if (!Input.anyKey) {
			vel.y = 0;
		}
		rb2d.velocity = vel;


	}
	// Use this for initialization
}

