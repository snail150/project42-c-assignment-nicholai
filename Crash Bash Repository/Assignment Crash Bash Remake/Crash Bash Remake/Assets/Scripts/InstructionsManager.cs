﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InstructionsManager : MonoBehaviour {


	private AudioSource musicClip;

	private void Awake()
	{
		DontDestroyOnLoad (transform.gameObject);
		musicClip = GetComponent<AudioSource> ();
	}
	// Use this for initialization
	void Start () {
		GameObject.FindGameObjectWithTag ("Music").GetComponent<InstructionsManager>().PlayMusic();
	}

	public void PinballScene(){
		SceneManager.LoadScene ("Pinball");
	}

	public void MenuScene(){
		SceneManager.LoadScene ("MainMenu");
	}

	public void Instructions2Scene(){
		SceneManager.LoadScene ("Instructions2");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayMusic(){
		if (musicClip.isPlaying)
			return;
		musicClip.Play ();
	}

	public void StopMusic(){
		musicClip.Stop ();
	}
}
