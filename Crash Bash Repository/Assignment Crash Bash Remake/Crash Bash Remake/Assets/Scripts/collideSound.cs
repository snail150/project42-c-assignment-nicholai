﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideSound : MonoBehaviour {

	public AudioClip hitSound;

	public AudioSource MusicSource;

	// Use this for initialization
	void Start () {
		MusicSource.clip = hitSound;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag.Contains("Ball")) {
			MusicSource.Play ();
		}
	}
		
}