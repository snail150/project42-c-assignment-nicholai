﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tileTrigger : MonoBehaviour {
	public GameObject myYellowTilePrefab;
	GameObject myYellowTileClone;
	public GameObject myGreenTilePrefab;
	GameObject myGreenTileClone;
	public GameObject myRedTilePrefab;
	GameObject myRedTileClone;
	public GameObject myBlueTilePrefab;
	GameObject myBlueTileClone;
	GameObject gameControllerr;
	GameObject neutralTile;


	// Use this for initialization
	void Start () {
		gameControllerr = GameObject.FindGameObjectWithTag ("GameController");
		neutralTile = GameObject.FindGameObjectWithTag ("Neutral");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "YellowPlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myYellowTileClone= Instantiate (myYellowTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

		if (collision.tag == "GreenPlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myGreenTileClone= Instantiate (myGreenTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

		if (collision.tag == "RedPlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myRedTileClone= Instantiate (myRedTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

		if (collision.tag == "BluePlayer") {
			float posx = this.transform.position.x;
			float posy = this.transform.position.y;
			Destroy (this.gameObject);
			myBlueTileClone= Instantiate (myBlueTilePrefab, transform.position = new Vector3 (posx, posy, -1), Quaternion.identity) as GameObject;
		}

	}

}
