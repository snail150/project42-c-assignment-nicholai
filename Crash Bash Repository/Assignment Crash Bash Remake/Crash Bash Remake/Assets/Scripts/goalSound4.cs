﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goalSound4 : MonoBehaviour {

	// Use this for initialization
	public AudioClip hitSound;

	public AudioSource MusicSource;

	// Use this for initialization
	void Start () {
		MusicSource.clip = hitSound;
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag.Contains("Ball")) {
			MusicSource.Play ();
		}
	}
}
