﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paintballMovement3 : MonoBehaviour {
	GameObject redPlayer;
	float defaultMove = 0f;
	float defaultNegMove = -0f;
	float defaultMove2 = 0f;
	float defaultNegMove2 = 0f;
	public float yellowXPos;

	public AudioClip hitSound;
	public GameObject musicSound;
	public GameObject musicSound2;
	public AudioSource MusicSource;
	public AudioSource MusicSource2;

	GameObject paintballMovement;
	GameObject paintballMovement2;
	GameObject paintballMovement4;
	// Use this for initialization
	void Start () {
		musicSound = GameObject.FindGameObjectWithTag ("BoxSound2");
		musicSound2 = GameObject.FindGameObjectWithTag ("BoxSound3");
		MusicSource = musicSound.GetComponent<AudioSource>();
		MusicSource2 = musicSound2.GetComponent<AudioSource>();
		redPlayer = GameObject.Find ("RedPlayer");
		paintballMovement = GameObject.FindGameObjectWithTag ("YellowPlayer");
		paintballMovement2 = GameObject.FindGameObjectWithTag ("GreenPlayer");
		paintballMovement4 = GameObject.FindGameObjectWithTag ("BluePlayer");
		StartCoroutine (stages ());
	}

	IEnumerator stages(){
		yield return new WaitForSeconds (4f);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
		defaultMove2 = 2.5f;
		defaultNegMove2 = -2.5f;


	}

	// Update is called once per frame
	void Update ()
	{
		yellowXPos = this.transform.position.x;
		if (Input.GetKey (KeyCode.L)) {
			if (redPlayer.transform.position.x <= 1400) {
				redPlayer.transform.Translate (defaultMove, 0f, 0f);
			} else {
				float posy = redPlayer.transform.position.y;
				redPlayer.transform.position = new Vector3 (1400f, posy, -1f);
			}		
		} else if (Input.GetKey (KeyCode.J)) {
			if (redPlayer.transform.position.x >= 560) {
				redPlayer.transform.Translate (defaultNegMove, 0f, 0f);
			} else {
				float posy = redPlayer.transform.position.y;
				redPlayer.transform.position = new Vector3 (560f, posy, -1f);
			}
		} else if (Input.GetKey (KeyCode.I)) {
			if (redPlayer.transform.position.y <= 948) {
				redPlayer.transform.Translate (0f, defaultMove, 0f);
			} else {
				float posx = redPlayer.transform.position.x;
				redPlayer.transform.position = new Vector3 (posx, 948f, -1f);
			}
		} else if (Input.GetKey (KeyCode.K)) {
			if (redPlayer.transform.position.y >= 108) {
				redPlayer.transform.Translate (0f, defaultNegMove, 0f);
			} else {
				float posx = redPlayer.transform.position.x;
				redPlayer.transform.position = new Vector3 (posx, 108f, -1f);
			}

		}
	}

	public void redStunner(){
		StartCoroutine (stunnedSpeed ());
	}

	IEnumerator timedSpeed(){
		defaultMove = 3.5f;
		defaultNegMove = -3.5f;
		yield return new WaitForSeconds (6);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	IEnumerator stunnedSpeed(){
		defaultMove = 0f;
		defaultNegMove = -0f;
		yield return new WaitForSeconds (3);
		defaultMove = 2.5f;
		defaultNegMove = -2.5f;
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "SpeedBoost") {
			MusicSource2.Play ();
			Destroy (collision.gameObject);
			StartCoroutine (timedSpeed ());
		}
		if (collision.tag == "StunMine") {
			MusicSource.Play ();
			Destroy (collision.gameObject);
			paintballMovement2.GetComponent<paintballMovement2>().yellowStunner();
			paintballMovement.GetComponent<paintballMovement>().greenStunner();
			paintballMovement4.GetComponent<paintballMovement4>().blueStunner();
		}
	}
}
