﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTop : MonoBehaviour {
	public KeyCode moveLeft = KeyCode.Home;
	public KeyCode moveRight = KeyCode.PageUp;
	public float speed = 10.0f;
	private Rigidbody2D rb2d;

	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update () {
		var vel = rb2d.velocity;
		if (Input.GetKey(moveRight)) {
			vel.x = speed;
		}
		else if (Input.GetKey(moveLeft)) {
			vel.x = -speed;
		}
		else if (!Input.anyKey) {
			vel.x = 0;
		}
		rb2d.velocity = vel;


	}
	// Use this for initialization
}

