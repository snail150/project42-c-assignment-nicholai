﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour {
	int greenScore;
	int yellowScore;
	int redScore;
	int blueScore;
	public GUISkin layout;
	GameObject neutralTileClone;
	public GameObject neutralTilePrefab;
	int countdownTimer = 90;

	// Use this for initialization
	void Start () {
		StartCoroutine (countdown ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator countdown(){
		yield return new WaitForSeconds (4f);
		for (int i = 0; i <= 90; i++) {
			countdownTimer--;
			if (countdownTimer == 0) {
				if (redScore > greenScore && redScore > blueScore && redScore > yellowScore) {
					SceneManager.LoadScene ("RedWins");
				} else if (yellowScore > greenScore && yellowScore > redScore && yellowScore > blueScore) {
					SceneManager.LoadScene ("YellowWins");
				} else if (greenScore > yellowScore && greenScore > redScore && greenScore > blueScore) {
					SceneManager.LoadScene ("GreenWins");
				} else {
					SceneManager.LoadScene ("BlueWins");
				}
			} else {
				yield return new WaitForSeconds (1f);
			}
		}


	}

	public void greenScoreAdd(){
		int lengthOfGreenTiles = GameObject.FindGameObjectsWithTag ("GreenTile").Length;
		Debug.Log (lengthOfGreenTiles);
		greenScore = greenScore + lengthOfGreenTiles;
		Debug.Log (greenScore);
		GameObject greenTiles;
		for (int i = 0; i < lengthOfGreenTiles; i++) {
			float posx = GameObject.FindGameObjectsWithTag ("GreenTile") [i].transform.position.x;
			float posy = GameObject.FindGameObjectsWithTag ("GreenTile") [i].transform.position.y;
			Destroy(greenTiles = GameObject.FindGameObjectsWithTag ("GreenTile")[i]);
			neutralTileClone= Instantiate (neutralTilePrefab, transform.position = new Vector3 (posx, posy, 0), Quaternion.identity) as GameObject;
		}
	}

	public void yellowScoreAdd(){
		int lengthOfYellowTiles = GameObject.FindGameObjectsWithTag ("YellowTile").Length;
		Debug.Log (lengthOfYellowTiles);
		yellowScore = yellowScore + lengthOfYellowTiles;
		Debug.Log (yellowScore);
		GameObject yellowTiles;
		for (int i = 0; i < lengthOfYellowTiles; i++) {
			float posx = GameObject.FindGameObjectsWithTag ("YellowTile") [i].transform.position.x;
			float posy = GameObject.FindGameObjectsWithTag ("YellowTile") [i].transform.position.y;
			Destroy(yellowTiles = GameObject.FindGameObjectsWithTag ("YellowTile")[i]);
			neutralTileClone= Instantiate (neutralTilePrefab, transform.position = new Vector3 (posx, posy, 0), Quaternion.identity) as GameObject;
		}
	}

	public void redScoreAdd(){
		int lengthOfRedTiles = GameObject.FindGameObjectsWithTag ("RedTile").Length;
		Debug.Log (lengthOfRedTiles);
		redScore = redScore + lengthOfRedTiles;
		Debug.Log (redScore);
		GameObject redTiles;	
		for (int i = 0; i < lengthOfRedTiles; i++) {
			float posx = GameObject.FindGameObjectsWithTag ("RedTile") [i].transform.position.x;
			float posy = GameObject.FindGameObjectsWithTag ("RedTile") [i].transform.position.y;
			Destroy(redTiles = GameObject.FindGameObjectsWithTag ("RedTile")[i]);
			neutralTileClone= Instantiate (neutralTilePrefab, transform.position = new Vector3 (posx, posy, 0), Quaternion.identity) as GameObject;
		}
	}

	public void blueScoreAdd(){
		int lengthOfBlueTiles = GameObject.FindGameObjectsWithTag ("BlueTile").Length;
		Debug.Log (lengthOfBlueTiles);
		blueScore = blueScore + lengthOfBlueTiles;
		Debug.Log (blueScore);
		GameObject blueTiles;	
		for (int i = 0; i < lengthOfBlueTiles; i++) {
			float posx = GameObject.FindGameObjectsWithTag ("BlueTile") [i].transform.position.x;
			float posy = GameObject.FindGameObjectsWithTag ("BlueTile") [i].transform.position.y;
			Destroy(blueTiles = GameObject.FindGameObjectsWithTag ("BlueTile")[i]);
			neutralTileClone= Instantiate (neutralTilePrefab, transform.position = new Vector3 (posx, posy, 0), Quaternion.identity) as GameObject;
		}
	}

	void OnGUI (){
		GUI.skin = layout;
		GUI.skin.label.fontSize = 40;
		GUI.contentColor = Color.black;
		GUI.Label (new Rect (Screen.width / 7 - 60 - 36, 115, 100, 100), "" + blueScore);
		GUI.Label (new Rect (Screen.width / 2 + 975 - 216, 115, 100, 100), "" + greenScore);
		GUI.Label (new Rect (Screen.width / 100 + 172 - 12, 855, 100, 100), "" + yellowScore);
		GUI.Label (new Rect (Screen.width / 22 + 1642 - 12, 855, 100, 100), "" + redScore);
		GUI.Label (new Rect (Screen.width / 22 + 1652 - 12, 427, 100, 100), "" + countdownTimer);
		GUI.Label (new Rect (Screen.width / 100 + 182 - 12, 427, 100, 100), "" + countdownTimer);


	}
}
