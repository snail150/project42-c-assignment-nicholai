﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxTrigger : MonoBehaviour {
	GameObject gameControllerr;
	public GameObject neutralTilePrefab;
	GameObject neutralTileClone;
	// Use this for initialization
	public GUISkin layout;
	public AudioClip hitSound;
	public GameObject musicSound;
	public AudioSource MusicSource;
	public int greenScore;
	public int yellowScore;
	GameObject paintballHUD;

	void Start () {
		musicSound = GameObject.FindGameObjectWithTag ("BoxSound");
		paintballHUD = GameObject.FindGameObjectWithTag ("Hud");

		MusicSource = musicSound.GetComponent<AudioSource>();
		gameControllerr = GameObject.FindGameObjectWithTag ("GameController");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.tag == "YellowPlayer") {
			MusicSource.Play ();
			paintballHUD.GetComponent<HUD> ().yellowScoreAdd ();
			Destroy (this.gameObject);
		}

		if (collision.tag == "GreenPlayer") {
			MusicSource.Play ();
			paintballHUD.GetComponent<HUD> ().greenScoreAdd ();
			Destroy (this.gameObject);
		}

		if (collision.tag == "RedPlayer") {
			MusicSource.Play ();
			paintballHUD.GetComponent<HUD> ().redScoreAdd ();
			Destroy (this.gameObject);
		}

		if (collision.tag == "BluePlayer") {
			MusicSource.Play ();
			paintballHUD.GetComponent<HUD> ().blueScoreAdd ();
			Destroy (this.gameObject);
		}
	}
}
