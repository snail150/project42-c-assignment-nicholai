﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour {
	public AudioClip hitSound;

	public AudioSource MusicSource;
	// Update is called once per frame

	void Start(){
		MusicSource.clip = hitSound;
	}
	void Update () {
		
	}

	public void clickExit(){
		MusicSource.Play ();
		Application.Quit ();

	}

	public void LoadPinballScene(){
		MusicSource.Play ();
		SceneManager.LoadScene ("Pinball");
	}

	public void LoadInstructionsScene(){
		MusicSource.Play ();
		SceneManager.LoadScene ("Instructions");
	}

	public void LoadPaintballGame(){
		SceneManager.LoadScene ("Paintball");
	}

	public void LoadPaintballScene(){
		MusicSource.Play ();
		SceneManager.LoadScene ("Instructions3");
	}

	public void LoadInstructions4Scene(){
		SceneManager.LoadScene ("Instructions4");
	}

	public void LoadInstructions5Scene(){
		SceneManager.LoadScene ("Instructions5");
	}

	public void PinballSingle(){
		SceneManager.LoadScene ("Pinball SinglePlayer");
	}

	public void PaintballSingle(){
		SceneManager.LoadScene ("Paintball SinglePlayer");
	}

	public void Website(){
		SceneManager.LoadScene ("HTML5");
	}

	public void WebsiteLaunch(){
		Application.OpenURL("http://crashbashremake.000webhostapp.com");
	}
}
